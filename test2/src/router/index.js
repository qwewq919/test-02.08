import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'

Vue.use(VueRouter)

const getRoutes = (route, prefix = '') => {
  let list = []

  route.forEach((item) => {
    if (item.prefix) {
      let child = getRoutes(item.child, item.prefix)
      list = [...list, ...child]
    } else {
      item.path = prefix + item.path
      list.push(item)
    }
  })

  return list
}

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: getRoutes(routes),
})




export default router
